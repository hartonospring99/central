FROM openjdk:19-jdk-alpine
EXPOSE 8080
ADD target/central.jar central.jar
ENTRYPOINT ["java", "-jar", "/central.jar"]

