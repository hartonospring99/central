package com.hartono.central.model;

import jakarta.persistence.*;

@Entity
@Table(name = "payment")
public class DataPayment {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "invoice")
    public String invoice = "";

    @Column(name = "type")
    public String type = "";

    @Column(name = "account")
    public String account = "";

    @Column(name = "description")
    public String description = "";

    @Column(name = "amount")
    public Integer amount = 0;

    @Column(name = "status")
    public String status;

    public DataPayment() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "DataPayment{" +
                "id='" + id + '\'' +
                ", invoice='" + invoice + '\'' +
                ", type='" + type + '\'' +
                ", account='" + account + '\'' +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", status='" + status + '\'' +
                '}';
    }
}
