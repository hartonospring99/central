package com.hartono.central.services;

import com.hartono.central.model.DataPayment;
import com.hartono.central.repository.PaymentRepository;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.vertx.core.json.JsonObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PaymentService {
    private PaymentRepository paymentRepository;
    @Value("${url.check.bin}")
    private String urlBin;

    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @CacheEvict(value = "getPayment")
    @Transactional
    public DataPayment getPayment(DataPayment payment) throws UnirestException, UnirestException {
        String bin = payment.getAccount().substring(0, 6);
        DataPayment input = new DataPayment();
        HttpResponse<String> cekBin = Unirest.get(urlBin)
                .routeParam("bin", bin)
                .asString();

        Integer responseCode = cekBin.getStatus();
        JsonObject collection = new JsonObject(cekBin.getBody());

        if (responseCode.equals(200)) {
            input.setType(collection.getString("type"));
            input.setAmount(payment.getAmount());
            input.setAccount(payment.getAccount());
            input.setDescription(payment.getDescription());
            input.setInvoice(payment.getInvoice());
            input.setStatus("SUCCESS");

            paymentRepository.save(input);

            return input;
        } else {
            throw new RuntimeException("TRANSAKSI GAGAL");
        }
    }

    @CacheEvict(value = "getStatusId", key = "#id")
    public Optional<DataPayment> getStatusById(String id) {
        return paymentRepository.findById(id);
    }
}
