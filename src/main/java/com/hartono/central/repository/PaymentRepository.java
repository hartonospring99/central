package com.hartono.central.repository;

import com.hartono.central.model.DataPayment;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PaymentRepository extends JpaRepository<DataPayment, String> {
}
