package com.hartono.central.controller;

import com.hartono.central.model.DataPayment;
import com.hartono.central.services.PaymentService;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@RestController
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PaymentController {
    private PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping(value = "/v1/payment")
    public ResponseEntity<?> getPayment(@RequestBody DataPayment payment) throws UnirestException {
        DataPayment resp = paymentService.getPayment(payment);
        return ResponseEntity.ok(resp);
    }

    @GetMapping(value = "/v1/status/{id}")
    public ResponseEntity<?> getStatusById(@PathVariable String id) throws UnirestException {

        Optional<DataPayment> data = paymentService.getStatusById(id);
        return new ResponseEntity<>(data,null, HttpStatus.OK);
    }
}
