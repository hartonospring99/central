CREATE DATABASE bincode OWNER developer;


-- DROP SCHEMA bin_schema;

CREATE SCHEMA bin_schema AUTHORIZATION developer;


-- Permissions

GRANT ALL ON SCHEMA bin_schema TO developer;


-- DROP SCHEMA payment_schema;

CREATE SCHEMA payment_schema AUTHORIZATION developer;


-- Permissions

GRANT ALL ON SCHEMA payment_schema TO developer;


-- bin_schema.bin definition

-- Drop table

-- DROP TABLE bin_schema.bin;

CREATE TABLE bin_schema.bin (
	bin varchar NULL,
	bank varchar NULL,
	card varchar NULL,
	bin_type varchar NULL,
	bin_level varchar NULL,
	country varchar NULL,
	country_code varchar NULL,
	website varchar NULL,
	phone varchar NULL,
	bin_valid varchar NULL
);

-- Permissions

ALTER TABLE bin_schema.bin OWNER TO developer;
GRANT ALL ON TABLE bin_schema.bin TO developer;

-- bin_schema.bin_invalid definition

-- Drop table

-- DROP TABLE bin_schema.bin_invalid;

CREATE TABLE bin_schema.bin_invalid (
	bin varchar NULL
);

-- Permissions

ALTER TABLE bin_schema.bin_invalid OWNER TO developer;
GRANT ALL ON TABLE bin_schema.bin_invalid TO developer;

-- payment_schema.payment definition

-- Drop table

-- DROP TABLE payment_schema.payment;

CREATE TABLE payment_schema.payment (
	id varchar NULL,
	invoice varchar NULL,
	"type" varchar NULL,
	account varchar NULL,
	description varchar NULL,
	amount int4 NULL,
	status varchar NULL
);

-- Permissions

ALTER TABLE payment_schema.payment OWNER TO developer;
GRANT ALL ON TABLE payment_schema.payment TO developer;


INSERT INTO bin_schema.bin (bin,bank,card,bin_type,bin_level,country,country_code,website,phone,bin_valid) VALUES
	 ('375531','BANK DANAMON','AMERICAN EXPRESS','CREDIT','PERSONAL GREEN CHARGE','INDONESIA','ID','http://www.danamon.co.id','(021) 3435 8888','true'),
	 ('400376','PT BANK MANDIRI (PERSERO) TBK','VISA','CREDIT','CLASSIC','INDONESIA','ID','http://www.bankmandiri.co.id','2152997777','true'),
	 ('530487','PT. BANK CIMB NIAGA TBK.','MASTERCARD','DEBIT','WORLD','INDONESIA','ID',NULL,NULL,'true'),
	 ('533659','BANK OF CHINA, LTD.','MASTERCARD','DEBIT','STANDARD','INDONESIA','ID',null,null,'true'),
	 ('540184','CITIBANK N.A.','MASTERCARD','CREDIT','PLATINUM','INDONESIA','ID',null,null,'true'),
	 ('541070','PT. BANK ANZ INDONESIA','MASTERCARD','CREDIT','TITANIUM','INDONESIA','ID',null,null,'true'),
	 ('555018','CITIBANK N.A.','MASTERCARD','CREDIT','PURCHASING','INDONESIA','ID',null,null,'true'),
	 ('451497','PT BANK UOB INDONESIA','VISA','DEBIT','ELECTRON','INDONESIA','ID','http://www.uob.co.id/','021 6983 0902-05','true'),
	 ('420194','PT BANK MEGA TBK.','VISA','CREDIT','PLATINUM','INDONESIA','ID','http://www.bankmega.com','2179175555','true'),
	 ('457508','PT BANK MEGA TBK.','VISA','CREDIT','BUSINESS','INDONESIA','ID','http://www.bankmega.com','2179175555','true');