=========== MINI PROJECT ================

Mini project ini adalah implementasi dari microservices architecture yang mana mengintegrasikan beberapa modul micro menjadi satu kesatuan aplikasi untuk bisa digunakan secara maksimal.

Dalam project ini, sekenarionya adalah :
1. Proses pembayaran dengan DEBIT / CREDIT CARD 
2. Proses check BIN (Bank Identification Number) kebutuhan untuk koleksi data BIN lengkap melalui integrasi dengan https://www.bincodes.com/.

Adapun teknologi yang diimplementasikan :
1. Springboot
2. PostgreSql
3. Redis
4. Docker
5. Postman
Dengan penerapan microservices.

===== RUNNING APPLICATION ==========
Untuk proses menjalankan aplikasi yang sudah siap pakai sebagai berikut:
1. Clone git repository central dan service-bin
2. Jalankan file app-compose.yml
        " docker compose -f app-compose.yml up -d "
   atau
	" docker-compose -f app-compose.yml up -d "
	
   *Pastikan sudah terinstall Docker dan Docker Compose
3. Setelah PostgreSql berjalan, lakukan CREATE Database
	CREATE DATABASE bincode OWNER developer;

4. Setelah itu lakukan CREATE Table dan Init database sesuai dengan file initDb.sql
5. Untuk melakukan testing silahkan import file Mini Collection.postman_collection.json ke Aplikasi Postman (Endpoint dan sample body sudah tersedia)
6. Squence diagram bisa dilihat pada file sequence.drawio
7. Untuk kebutuhan testing secara REAL harus mempunyai apiKey dari https://www.bincodes.com/
8. Untuk kebutuhan demo silahkan masukan nomor BIN dengan diawali angka 9 (sembilan)

	{
    	   "valid": "true",
    	   "country": "UNITED STATES",
    	   "bank": "CITIBANK, N.A.",
    	   "website": "HTTPS://ONLINE.CITIBANK.COM/",
    	   "level": "WORLD CARD",
    	   "phone": "1-800-374-9700",
    	   "bin": "975531",
    	   "countryCode": "US",
    	   "type": "CREDIT",
    	   "card": "MASTERCARD"
	}
	
==========  PROSES DATA BIN  ===================
Pada modul service-bin memiliki proses data sebagai berikut :
1. Load data dari database ketika applikasi running pertama kali
2. Saat transaksional :
 	- Jika BIN ada di redis akan ditampilkan data dari redis
 	- Jika tidak ada di redis maka akan mengambil di memory (HashMap) dan ditampilkan
 	- Jika tidak ada di memory maka akan mengambil di Database dan ditampilkan
 		selanjutnya data akan disimpan di memory dan redis
 	- Jika tidak ada di Database maka akan HIT API https://www.bincodes.com/ untuk mendapatkan data BIN
 	- Jika valid maka akan disimpan di Database, disimpan di memory, disimpan di redis dan selanjutnya ditampilkan datanya.
 	- Jika tidak valid maka akan disimpan di Database, disimpan di memory, disimpan di redis dan selanjutnya pesan error.
 		(Jadi jika nantinya ada BIN yang sama akan langsung dikembalikan pesan error)


========= PROSES DATA PAYMENT ==============
Dalam proses ini sekenarionya ada user melakukan pembayaran dengan debit/credit card sebagai berikut :
1. User mengirim data pembayaran
2. Service Central mengolah data BIN terlebih dahulu
	Jika valid lanjut transaksi jika tidak valid maka transaksi gagal.


Fokus dari Mini Project ini adalah pada proses penyediaan data BIN dari pihak https://www.bincodes.com/, 
sedangkan untuk proses di service Central sebagai salah satu microservice yang mengkonsumsi data dari microservice lainnya.
 	
 	
 	
 	
 	
 	
 	
 	
